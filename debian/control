Source: libtext-diff-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Salvatore Bonaccorso <carnil@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libalgorithm-diff-perl <!nocheck>,
                     libtest-cpan-meta-perl <!nocheck>,
                     libtest-minimumversion-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtext-diff-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtext-diff-perl.git
Homepage: https://metacpan.org/release/Text-Diff
Rules-Requires-Root: no

Package: libtext-diff-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libalgorithm-diff-perl
Description: Perl module to find differences in files and record sets
 Text::Diff provides a basic functions similar to the GNU diff utility. It is
 not anywhere near as feature complete as GNU diff, but is better integrated
 with Perl and available on all platforms. It is often faster than shelling
 out to a system's diff executable for small files, and generally slower on
 larger files. While it is not guaranteed to produce the same output as the
 system diff command, output is often identical.
